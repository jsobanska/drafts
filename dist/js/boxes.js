
$('.status[data-id="0"]').each(function(i,el) {
    $(el).addClass('grey'); 
    $(el).find('span').html('not assigned to any setup and connection not verified')  
     $(el).find('p').html('not assigned to any setup and connection not verified')  
});

$('.status[data-id="1"]').each(function(i,el) {
    $(el).addClass('lightblue');
    $(el).find('span').html('not assigned to any setup but connection verified')
    $(el).find('p').html('not assigned to any setup but connection verified')
});

$('.status[data-id="2"]').each(function(i,el) {
    $(el).addClass('darkblue');
    $(el).find('span').html('not assigned to any setup but allowed to transfer data')
    $(el).find('p').html('not assigned to any setup but allowed to transfer data')
});

$('.status[data-id="3"]').each(function(i,el) {
    $(el).addClass('lightgreen');
    $(el).find('span').html('assigned to a setup but not allowed to transfer data')
    $(el).find('p').html('assigned to a setup but not allowed to transfer data')
});

$('.status[data-id="4"]').each(function(i,el) {
    $(el).addClass('darkgreen');
    $(el).find('span').html('assigned to a setup and allowed to transfer data')
     $(el).find('p').html('assigned to a setup and allowed to transfer data')
});

$('.status[data-id="5"]').each(function(i,el) {
    $(el).addClass('lightred');
    $(el).find('span').html('error')
    $(el).find('p').html('error')
});

$('.status[data-id="6"]').each(function(i,el) {
    $(el).addClass('mediumred');
    $(el).find('span').html('requires manual intervention')
    $(el).find('p').html('requires manual intervention')
});

$('.status[data-id="7"]').each(function(i,el) {
    $(el).addClass('darkred');
    $(el).find('span').html('to replace')
    $(el).find('p').html('to replace')
});


  $(".dropdown-menu.company li a").click(function(){
    $("#btn-status:first-child").text($(this).text());
    $("#btn-status:first-child").val($(this).text());
  });