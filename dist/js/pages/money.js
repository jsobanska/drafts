

  var ranges = [["Jan", 70, 100], ["Feb", 137, 196], ["Mar", 201, 288], ["Apr", 260, 371], ["May", 312, 446], ["Jun", 362, 517], ["Jul", 411, 588], ["Aug", 458, 654], ["Sep", 510, 729], ["Oct", 569, 813], ["Nov", 633, 904], ["Dec", 700, 1000]],
      bills_with_battery = [["Jan", 70], ["Feb", 137], ["Mar", 201], ["Apr", 260], ["May", 312], ["Jun", 362], ["Jul", 411], ["Aug", 458], ["Sep", 510], ["Oct", 569], ["Nov", 633], ["Dec", 700]],
      bills_without_battery = [["Jan", 100], ["Feb", 196], ["Mar", 288], ["Apr", 371], ["May", 446], ["Jun", 517], ["Jul", 588], ["Aug", 654], ["Sep", 729], ["Oct", 813], ["Nov", 904], ["Dec", 1000]];

  var energy_bills = Highcharts.chart('energy-bills', {
    colors: ['#029676','#549e39','#c0cf3a','#8ab833'],
        chart: {

        backgroundColor: null
    },
    title: {
      text: "Cumulative Energy Bill"
    },
    xAxis: {
      categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    },
    yAxis: {
    gridLineColor: '#bbc3bd',
      gridLineDashStyle: 'longdash',
      gridLineWidth: 1,
      title: {
        text: "Value of bill [CHF]"
      },
      labels: {
        align: "left",
        x: 0,
        y: -3
      }
    },
    tooltip: {
      crosshairs: true,
      shared: true,
      valueSuffix: 'CHF'
    },
    series: [{
      name: 'Bills with WATT3 platform',
      data: bills_with_battery,
      zIndex: 1,
      lineWidth: 3,
      color: "#0abe50"
    }, {
      name: 'Bills without WATT3 platform',
      data: bills_without_battery,
      zIndex: 1,
      lineWidth: 3
    }, {
      name: 'Range',
      data: ranges,
      type: 'arearange',
      lineWidth: 0,
      linkedTo: ':previous',
      color: "#0abe50",
      fillOpacity: 0.3,
      zIndex: 0
    }]
  });
