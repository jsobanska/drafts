$("#spinner").show();

$.getJSON("data/residential.json", function(json) {
    var array = json

    $("#spinner").hide();
var production =[];
var consumption  = [];
var autoconsumption = [];
var bought = [];
var bought2 = [];
var sold = [];
var soc= [];
var charge = [];
var discharge = [];


var d = new Date();
var d_unix = 0;

for (var k = 0; k < array.length; k++) {
	d =  new Date(array[k][0]);
	d_unix = d.getTime();

	soc.push([d_unix, array[k][1]]);
	charge.push([d_unix, array[k][3]]);
	discharge.push([d_unix, array[k][4]]);		
	production.push([d_unix, array[k][5]]);
	consumption.push([d_unix, array[k][6]]);
	bought.push([d_unix, -array[k][7]]);
  bought2.push([d_unix, array[k][7]]);
	sold.push([d_unix, array[k][8]]);
  autoconsumption.push([d_unix, Math.min(array[k][5],array[k][6])]);

}

var seriesOptions = [],
    seriesCounter = 0,
    names = ['Consumption', 'Production', 'Bought', 'Sold'],
    energy_output_tab =[consumption, production, bought, sold];


function sortFunction(a, b) {
  if (a[0] === b[0]) {
    return 0;
  } else {
    return a[0] - b[0];
  }
}

function option_energy(i, name) {
  var tab = energy_output_tab[i],
      type = 'line',
      approximation = "average";
  if (name == 'Bought') {
    type = 'column';
  }
  else if (name == 'Sold') {
    type = 'column';
  }

  tab.sort(sortFunction);
  var legendNames = ['Consumption (Avg)', 'Production (Avg)', 'Bought (Avg)', 'Sold (Avg)'];
  seriesOptions[i] = {
    name: legendNames[i],
    data: tab,
    type: type,
    dataGrouping: {
      approximation: approximation
    }
  };
};

function createChart() {
  var energy = Highcharts.stockChart('energy_output_residential', {
    colors: ['#029676','#c0cf3a','#8ab833','#549e39','#bbc3bd','#8f9191','#e3ded1'],
    chart :{
       backgroundColor: null
    },
    title: {
      text: 'Output for energy'
     
    },
    rangeSelector: {
      selected: 5
    },
    yAxis: {
      gridLineColor: '#bbc3bd',
      gridLineDashStyle: 'longdash',
      gridLineWidth: 1,
      title: {
        text: 'Energy[kWh]'
      },
      labels: {
        format: '{value} kWh'
      },
      opposite: false
    },
    plotOptions: {
      series: {
        showInNavigator: true
      }
    },
    tooltip: {
      valueDecimals: 2,
      valueSuffix: ' kWh',
      split: true
    },
    legend: {
      enabled: true
    },
    credits: {
        enabled: false
      },
    navigation: {
        buttonOptions: {
            enabled: false
        }
    },
    series: seriesOptions
  });
}

for (i = 0; i < names.length; i++) {
  name = names[i];
  option_energy(i, name);
}

var seriesOptionsBattery = [],
    seriesCounterBattery = 0,
    namesBattery = ["Charge", "Discharge", "SOC"];
var battery_tab= [charge, discharge, soc];




function createChartForBattery(div, option) {
  
  var battery = Highcharts.stockChart(div, {
    colors: ['#029676','#c0cf3a','#8ab833','#549e39','#bbc3bd','#8f9191','#e3ded1'],
    chart :{
       backgroundColor: null
    },
    title: {
      text: 'Output for battery'
    },

    rangeSelector: {
      selected: 5
    },
    yAxis: [{
      gridLineColor: '#bbc3bd',
      gridLineDashStyle: 'longdash',
      gridLineWidth: 1,
      title: {
        text: 'Energy[kWh]'
      },
      labels: {
        format: '{value} kWh'
      },
      opposite: false
    }],
    plotOptions: {
      series: {
        showInNavigator: true
      }
    },
    tooltip: {
      valueDecimals: 2,
      split: true
    },
    legend: {
      enabled: true
    },
    credits: {
        enabled: false
      },
    navigation: {
        buttonOptions: {
            enabled: false
        }
    },
    series: option
  });
}

function batteryOption(i, name) {
  var tab = battery_tab[i],
      type = 'column',
      approximation = "sum";
     if (name == "SOC") {
      approximation = "high";
      type='line'
    }
  tab.sort(sortFunction);
  var legendNames = [ 'Charge (Sum) ', 'Discharge (Sum)', 'SOC (Max)'];
  seriesOptionsBattery[i] = {
    name: legendNames[i],
    data: tab,
    type: type,
    dataGrouping: {
      approximation: approximation
    }
  };
}

for (var i = 0; i < namesBattery.length; i++) {
  var name = namesBattery[i];
  batteryOption(i, name);
}


$(function () {
  createChart();
  createChartForBattery("soc_residential", seriesOptionsBattery);
});


});