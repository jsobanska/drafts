

  var streams_revenue = Highcharts.chart('streams_revenue', {
    colors: ['#029676','#c0cf3a','#8ab833','#549e39','#bbc3bd','#8f9191','#e3ded1','#C5DC9A'],
    chart: {
      type: 'column',
      backgroundColor: null
    },
    title: {
      text: 'EnergyBank Product - Revenue Streams'
    },
    xAxis: {
      categories: ['Penny', 'Eco', 'Paradise']
    },
    yAxis: {
        gridLineColor: '#bbc3bd',
      gridLineDashStyle: 'longdash',
      gridLineWidth: 1,
      min: 0,
      title: {
        text: 'Total Revenue [CHF]'
      },
      stackLabels: {
        enabled: true,
        style: {
          fontWeight: 'bold',
          color: Highcharts.theme && Highcharts.theme.textColor || 'gray'
        }
      }
    },
    tooltip: {
      headerFormat: '<b>{point.x}</b><br/>',
      pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
    },
    plotOptions: {
      column: {
        stacking: 'normal'
      }
    },
     credits: {
        enabled: false
      },
    navigation: {
        buttonOptions: {
            enabled: false
        }
    },
    series: [{
      name: 'Community Network',
      data: [0, 0, 300]
    }, {
      name: 'PV lopping',
      data: [0, 0, 250]
    }, {
      name: 'Tertiary Ancillary Service',
      data: [0, 200, 200]
    }, {
      name: 'Secondary Ancillary Service',
      data: [0, 300, 300]
    }, {
      name: 'Primary Ancillary Service',
      data: [0, 500, 500]
    }, {
      name: 'Peak Shaving',
      data: [100, 100, 100]
    }, {
      name: 'Demand Charge',
      data: [200, 200, 200]
    }, {
      name: 'Market Arbitrage',
      data: [300, 300, 300]
    }]
  });
