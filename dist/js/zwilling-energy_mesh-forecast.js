var consumption_real = [{
  "date": "2016-04-20 00:00",
  "consumption": 17.2,
  "average_profile": 15.61
}, {
  "date": "2016-04-20 01:00",
  "consumption": 17.2,
  "average_profile": 16.64
}, {
  "date": "2016-04-20 02:00",
  "consumption": 18.3,
  "average_profile": 17.46
}, {
  "date": "2016-04-20 03:00",
  "consumption": 21.2,
  "average_profile": 21.22
}, {
  "date": "2016-04-20 04:00",
  "consumption": 21.3,
  "average_profile": 20.95
}, {
  "date": "2016-04-20 05:00",
  "consumption": 27,
  "average_profile": 24.22
}, {
  "date": "2016-04-20 06:00",
  "consumption": 27.8,
  "average_profile": 25.12
}, {
  "date": "2016-04-20 07:00",
  "consumption": 25.2,
  "average_profile": 25.02
}, {
  "date": "2016-04-20 08:00",
  "consumption": 25.2,
  "average_profile": 23.33
}, {
  "date": "2016-04-20 09:00",
  "consumption": 28.2,
  "average_profile": 26.32
}, {
  "date": "2016-04-20 10:00",
  "consumption": 28.5,
  "average_profile": 24.46
}, {
  "date": "2016-04-20 11:00",
  "consumption": 23,
  "average_profile": 24.08
}, {
  "date": "2016-04-20 12:00",
  "consumption": 24,
  "average_profile": 22.12
}, {
  "date": "2016-04-20 13:00",
  "consumption": 27.5,
  "average_profile": 23.35
}, {
  "date": "2016-04-20 14:00",
  "consumption": 23.4,
  "average_profile": 24.02
}, {
  "date": "2016-04-20 15:00",
  "consumption": 23.1,
  "average_profile": 21.65
}, {
  "date": "2016-04-20 16:00",
  "consumption": 22,
  "average_profile": 19.95
}, {
  "date": "2016-04-20 17:00",
  "consumption": 30.9,
  "average_profile": 24.86
}, {
  "date": "2016-04-20 18:00",
  "consumption": 25.4,
  "average_profile": 21.52
}, {
  "date": "2016-04-20 19:00",
  "consumption": 23.1,
  "average_profile": 20.24
}, {
  "date": "2016-04-20 20:00",
  "consumption": 18,
  "average_profile": 17.79
}, {
  "date": "2016-04-20 21:00",
  "consumption": 19,
  "average_profile": 16.94
}, {
  "date": "2016-04-20 22:00",
  "consumption": 17.5,
  "average_profile": 16.39
}, {
  "date": "2016-04-20 23:00",
  "consumption": 16.5,
  "average_profile": 16.51
}, {
  "date": "2016-04-21 00:00",
  "consumption": 14.2,
  "average_profile": 15.61
}, {
  "date": "2016-04-21 01:00",
  "consumption": 17.7,
  "average_profile": 16.64
}, {
  "date": "2016-04-21 02:00",
  "consumption": 17.1,
  "average_profile": 17.46
}, {
  "date": "2016-04-21 03:00",
  "consumption": 27,
  "average_profile": 21.22
}, {
  "date": "2016-04-21 04:00",
  "consumption": 23.5,
  "average_profile": 20.95
}, {
  "date": "2016-04-21 05:00",
  "consumption": 28.2,
  "average_profile": 24.22
}, {
  "date": "2016-04-21 06:00",
  "consumption": 28.3,
  "average_profile": 25.12
}, {
  "date": "2016-04-21 07:00",
  "consumption": 28.3,
  "average_profile": 25.02
}, {
  "date": "2016-04-21 08:00",
  "consumption": 28.7,
  "average_profile": 23.33
}, {
  "date": "2016-04-21 09:00",
  "consumption": 29.1,
  "average_profile": 26.32
}, {
  "date": "2016-04-21 10:00",
  "consumption": 28.8,
  "average_profile": 24.46
}, {
  "date": "2016-04-21 11:00",
  "consumption": 23.9,
  "average_profile": 24.08
}, {
  "date": "2016-04-21 12:00",
  "consumption": 30.9,
  "average_profile": 22.12
}, {
  "date": "2016-04-21 13:00",
  "consumption": 24.7,
  "average_profile": 23.35
}, {
  "date": "2016-04-21 14:00",
  "consumption": 32.4,
  "average_profile": 24.02
}, {
  "date": "2016-04-21 15:00",
  "consumption": 22.7,
  "average_profile": 21.65
}, {
  "date": "2016-04-21 16:00",
  "consumption": 22.2,
  "average_profile": 19.95
}, {
  "date": "2016-04-21 17:00",
  "consumption": 33.4,
  "average_profile": 24.86
}, {
  "date": "2016-04-21 18:00",
  "consumption": 25.4,
  "average_profile": 21.52
}, {
  "date": "2016-04-21 19:00",
  "consumption": 24.5,
  "average_profile": 20.24
}, {
  "date": "2016-04-21 20:00",
  "consumption": 20.8,
  "average_profile": 17.79
}, {
  "date": "2016-04-21 21:00",
  "consumption": 18.3,
  "average_profile": 16.94
}, {
  "date": "2016-04-21 22:00",
  "consumption": 18.9,
  "average_profile": 16.39
}, {
  "date": "2016-04-21 23:00",
  "consumption": 16.8,
  "average_profile": 16.51
}, {
  "date": "2016-04-22 00:00",
  "consumption": 19.3,
  "average_profile": 15.61
}, {
  "date": "2016-04-22 01:00",
  "consumption": 18.5,
  "average_profile": 16.64
}, {
  "date": "2016-04-22 02:00",
  "consumption": 20.5,
  "average_profile": 17.46
}, {
  "date": "2016-04-22 03:00",
  "consumption": 23.9,
  "average_profile": 21.22
}, {
  "date": "2016-04-22 04:00",
  "consumption": 26.5,
  "average_profile": 20.95
}, {
  "date": "2016-04-22 05:00",
  "consumption": 28.5,
  "average_profile": 24.22
}, {
  "date": "2016-04-22 06:00",
  "consumption": 28.9,
  "average_profile": 25.12
}, {
  "date": "2016-04-22 07:00",
  "consumption": 28.6,
  "average_profile": 25.02
}, {
  "date": "2016-04-22 08:00",
  "consumption": 28.5,
  "average_profile": 23.33
}, {
  "date": "2016-04-22 09:00",
  "consumption": 28.8,
  "average_profile": 26.32
}, {
  "date": "2016-04-22 10:00",
  "consumption": 29.8,
  "average_profile": 24.46
}, {
  "date": "2016-04-22 11:00",
  "consumption": 30.4,
  "average_profile": 24.08
}, {
  "date": "2016-04-22 12:00",
  "consumption": 23.5,
  "average_profile": 22.12
}, {
  "date": "2016-04-22 13:00",
  "consumption": 25.1,
  "average_profile": 23.35
}, {
  "date": "2016-04-22 14:00",
  "consumption": 31.3,
  "average_profile": 24.02
}, {
  "date": "2016-04-22 15:00",
  "consumption": 24,
  "average_profile": 21.65
}, {
  "date": "2016-04-22 16:00",
  "consumption": 23.7,
  "average_profile": 19.95
}, {
  "date": "2016-04-22 17:00",
  "consumption": 31.3,
  "average_profile": 24.86
}, {
  "date": "2016-04-22 18:00",
  "consumption": 22.9,
  "average_profile": 21.52
}, {
  "date": "2016-04-22 19:00",
  "consumption": 23.4,
  "average_profile": 20.24
}, {
  "date": "2016-04-22 20:00",
  "consumption": 20,
  "average_profile": 17.79
}, {
  "date": "2016-04-22 21:00",
  "consumption": 18.8,
  "average_profile": 16.94
}, {
  "date": "2016-04-22 22:00",
  "consumption": 15.8,
  "average_profile": 16.39
}, {
  "date": "2016-04-22 23:00",
  "consumption": 17.8,
  "average_profile": 16.51
}, {
  "date": "2016-04-23 00:00",
  "consumption": 15.6,
  "average_profile": 15.61
}, {
  "date": "2016-04-23 01:00",
  "consumption": 16.9,
  "average_profile": 16.64
}, {
  "date": "2016-04-23 02:00",
  "consumption": 21.6,
  "average_profile": 17.46
}, {
  "date": "2016-04-23 03:00",
  "consumption": 19,
  "average_profile": 21.22
}, {
  "date": "2016-04-23 04:00",
  "consumption": 22.3,
  "average_profile": 20.95
}, {
  "date": "2016-04-23 05:00",
  "consumption": 21.4,
  "average_profile": 24.22
}, {
  "date": "2016-04-23 06:00",
  "consumption": 26.5,
  "average_profile": 25.12
}, {
  "date": "2016-04-23 07:00",
  "consumption": 25.1,
  "average_profile": 25.02
}, {
  "date": "2016-04-23 08:00",
  "consumption": 22.2,
  "average_profile": 23.33
}, {
  "date": "2016-04-23 09:00",
  "consumption": 28.5,
  "average_profile": 26.32
}, {
  "date": "2016-04-23 10:00",
  "consumption": 20.5,
  "average_profile": 24.46
}, {
  "date": "2016-04-23 11:00",
  "consumption": 21.3,
  "average_profile": 24.08
}, {
  "date": "2016-04-23 12:00",
  "consumption": 22.4,
  "average_profile": 22.12
}, {
  "date": "2016-04-23 13:00",
  "consumption": 28.1,
  "average_profile": 23.35
}, {
  "date": "2016-04-23 14:00",
  "consumption": 22.1,
  "average_profile": 24.02
}, {
  "date": "2016-04-23 15:00",
  "consumption": 20.1,
  "average_profile": 21.65
}, {
  "date": "2016-04-23 16:00",
  "consumption": 15.8,
  "average_profile": 19.95
}, {
  "date": "2016-04-23 17:00",
  "consumption": 16,
  "average_profile": 24.86
}, {
  "date": "2016-04-23 18:00",
  "consumption": 15.3,
  "average_profile": 21.52
}, {
  "date": "2016-04-23 19:00",
  "consumption": 18.5,
  "average_profile": 20.24
}, {
  "date": "2016-04-23 20:00",
  "consumption": 17.9,
  "average_profile": 17.79
}, {
  "date": "2016-04-23 21:00",
  "consumption": 17,
  "average_profile": 16.94
}, {
  "date": "2016-04-23 22:00",
  "consumption": 16.6,
  "average_profile": 16.39
}, {
  "date": "2016-04-23 23:00",
  "consumption": 16.1,
  "average_profile": 16.51
}, {
  "date": "2016-04-24 00:00",
  "consumption": 14.7,
  "average_profile": 15.61
}, {
  "date": "2016-04-24 01:00",
  "consumption": 16.6,
  "average_profile": 16.64
}, {
  "date": "2016-04-24 02:00",
  "consumption": 17.5,
  "average_profile": 17.46
}, {
  "date": "2016-04-24 03:00",
  "consumption": 18.7,
  "average_profile": 21.22
}, {
  "date": "2016-04-24 04:00",
  "consumption": 17.5,
  "average_profile": 20.95
}, {
  "date": "2016-04-24 05:00",
  "consumption": 16.9,
  "average_profile": 24.22
}, {
  "date": "2016-04-24 06:00",
  "consumption": 15.9,
  "average_profile": 25.12
}, {
  "date": "2016-04-24 07:00",
  "consumption": 17.8,
  "average_profile": 25.02
}, {
  "date": "2016-04-24 08:00",
  "consumption": 16.1,
  "average_profile": 23.33
}, {
  "date": "2016-04-24 09:00",
  "consumption": 17.6,
  "average_profile": 26.32
}, {
  "date": "2016-04-24 10:00",
  "consumption": 17.3,
  "average_profile": 24.46
}, {
  "date": "2016-04-24 11:00",
  "consumption": 16.6,
  "average_profile": 24.08
}, {
  "date": "2016-04-24 12:00",
  "consumption": 15.7,
  "average_profile": 22.12
}, {
  "date": "2016-04-24 13:00",
  "consumption": 19.7,
  "average_profile": 23.35
}, {
  "date": "2016-04-24 14:00",
  "consumption": 16.2,
  "average_profile": 24.02
}, {
  "date": "2016-04-24 15:00",
  "consumption": 14.6,
  "average_profile": 21.65
}, {
  "date": "2016-04-24 16:00",
  "consumption": 16.5,
  "average_profile": 19.95
}, {
  "date": "2016-04-24 17:00",
  "consumption": 14.8,
  "average_profile": 24.86
}, {
  "date": "2016-04-24 18:00",
  "consumption": 14,
  "average_profile": 21.52
}, {
  "date": "2016-04-24 19:00",
  "consumption": 16.5,
  "average_profile": 20.24
}, {
  "date": "2016-04-24 20:00",
  "consumption": 18,
  "average_profile": 17.79
}, {
  "date": "2016-04-24 21:00",
  "consumption": 16,
  "average_profile": 16.94
}, {
  "date": "2016-04-24 22:00",
  "consumption": 14.2,
  "average_profile": 16.39
}, {
  "date": "2016-04-24 23:00",
  "consumption": 14.6,
  "average_profile": 16.51
}, {
  "date": "2016-04-25 00:00",
  "consumption": 13.3,
  "average_profile": 15.61
}, {
  "date": "2016-04-25 01:00",
  "consumption": 15,
  "average_profile": 16.64
}, {
  "date": "2016-04-25 02:00",
  "consumption": 14.9,
  "average_profile": 17.46
}, {
  "date": "2016-04-25 03:00",
  "consumption": 20.5,
  "average_profile": 21.22
}, {
  "date": "2016-04-25 04:00",
  "consumption": 20.2,
  "average_profile": 20.95
}, {
  "date": "2016-04-25 05:00",
  "consumption": 22.5,
  "average_profile": 24.22
}, {
  "date": "2016-04-25 06:00",
  "consumption": 26.6,
  "average_profile": 25.12
}, {
  "date": "2016-04-25 07:00",
  "consumption": 22.6,
  "average_profile": 25.02
}, {
  "date": "2016-04-25 08:00",
  "consumption": 23.3,
  "average_profile": 23.33
}, {
  "date": "2016-04-25 09:00",
  "consumption": 24.8,
  "average_profile": 26.32
}, {
  "date": "2016-04-25 10:00",
  "consumption": 21.3,
  "average_profile": 24.46
}, {
  "date": "2016-04-25 11:00",
  "consumption": 25.6,
  "average_profile": 24.08
}, {
  "date": "2016-04-25 12:00",
  "consumption": 21.5,
  "average_profile": 22.12
}, {
  "date": "2016-04-25 13:00",
  "consumption": 23.2,
  "average_profile": 23.35
}, {
  "date": "2016-04-25 14:00",
  "consumption": 20.7,
  "average_profile": 24.02
}, {
  "date": "2016-04-25 15:00",
  "consumption": 21.4,
  "average_profile": 21.65
}, {
  "date": "2016-04-25 16:00",
  "consumption": 17.7,
  "average_profile": 19.95
}, {
  "date": "2016-04-25 17:00",
  "consumption": 23.9,
  "average_profile": 24.86
}, {
  "date": "2016-04-25 18:00",
  "consumption": 21,
  "average_profile": 21.52
}, {
  "date": "2016-04-25 19:00",
  "consumption": 17.2,
  "average_profile": 20.24
}, {
  "date": "2016-04-25 20:00",
  "consumption": 17.2,
  "average_profile": 17.79
}, {
  "date": "2016-04-25 21:00",
  "consumption": 16.5,
  "average_profile": 16.94
}, {
  "date": "2016-04-25 22:00",
  "consumption": 14.7,
  "average_profile": 16.39
}, {
  "date": "2016-04-25 23:00",
  "consumption": 14.8,
  "average_profile": 16.51
}, {
  "date": "2016-04-26 00:00",
  "consumption": 14.9,
  "average_profile": 15.61
}, {
  "date": "2016-04-26 01:00",
  "consumption": 16.3,
  "average_profile": 16.64
}, {
  "date": "2016-04-26 02:00",
  "consumption": 17.4,
  "average_profile": 17.46
}, {
  "date": "2016-04-26 03:00",
  "consumption": 20.4,
  "average_profile": 21.22
}, {
  "date": "2016-04-26 04:00",
  "consumption": 21.8,
  "average_profile": 20.95
}, {
  "date": "2016-04-26 05:00",
  "consumption": 24.4,
  "average_profile": 24.22
}, {
  "date": "2016-04-26 06:00",
  "consumption": 26.3,
  "average_profile": 25.12
}, {
  "date": "2016-04-26 07:00",
  "consumption": 27.6,
  "average_profile": 25.02
}, {
  "date": "2016-04-26 08:00",
  "consumption": 21.5,
  "average_profile": 23.33
}, {
  "date": "2016-04-26 09:00",
  "consumption": 27.6,
  "average_profile": 26.32
}, {
  "date": "2016-04-26 10:00",
  "consumption": 22.2,
  "average_profile": 24.46
}, {
  "date": "2016-04-26 11:00",
  "consumption": 27.5,
  "average_profile": 24.08
}, {
  "date": "2016-04-26 12:00",
  "consumption": 18.4,
  "average_profile": 22.12
}, {
  "date": "2016-04-26 13:00",
  "consumption": 20.5,
  "average_profile": 23.35
}, {
  "date": "2016-04-26 14:00",
  "consumption": 20.6,
  "average_profile": 24.02
}, {
  "date": "2016-04-26 15:00",
  "consumption": 21.3,
  "average_profile": 21.65
}, {
  "date": "2016-04-26 16:00",
  "consumption": 19,
  "average_profile": 19.95
}, {
  "date": "2016-04-26 17:00",
  "consumption": 22.8,
  "average_profile": 24.86
}, {
  "date": "2016-04-26 18:00",
  "consumption": 17.9,
  "average_profile": 21.52
}, {
  "date": "2016-04-26 19:00",
  "consumption": 19.3,
  "average_profile": 20.24
}, {
  "date": "2016-04-26 20:00",
  "consumption": 17.4,
  "average_profile": 17.79
}, {
  "date": "2016-04-26 21:00",
  "consumption": 17.2,
  "average_profile": 16.94
}, {
  "date": "2016-04-26 22:00",
  "consumption": 15.2,
  "average_profile": 16.39
}, {
  "date": "2016-04-26 23:00",
  "consumption": 14.6,
  "average_profile": 16.51
}, {
  "date": "2016-04-27 00:00",
  "consumption": 14,
  "average_profile": 15.61
}, {
  "date": "2016-04-27 01:00",
  "consumption": 15.4,
  "average_profile": 16.64
}, {
  "date": "2016-04-27 02:00",
  "consumption": 15.7,
  "average_profile": 17.46
}, {
  "date": "2016-04-27 03:00",
  "consumption": 20.4,
  "average_profile": 21.22
}, {
  "date": "2016-04-27 04:00",
  "consumption": 18.3,
  "average_profile": 20.95
}, {
  "date": "2016-04-27 05:00",
  "consumption": 26.5,
  "average_profile": 24.22
}, {
  "date": "2016-04-27 06:00",
  "consumption": 22,
  "average_profile": 25.12
}, {
  "date": "2016-04-27 07:00",
  "consumption": 26.9,
  "average_profile": 25.02
}, {
  "date": "2016-04-27 08:00",
  "consumption": 22.7,
  "average_profile": 23.33
}, {
  "date": "2016-04-27 09:00",
  "consumption": 25.1,
  "average_profile": 26.32
}, {
  "date": "2016-04-27 10:00",
  "consumption": 20.8,
  "average_profile": 24.46
}, {
  "date": "2016-04-27 11:00",
  "consumption": 26.7,
  "average_profile": 24.08
}, {
  "date": "2016-04-27 12:00",
  "consumption": 21,
  "average_profile": 22.12
}, {
  "date": "2016-04-27 13:00",
  "consumption": 20.1,
  "average_profile": 23.35
}, {
  "date": "2016-04-27 14:00",
  "consumption": 21.1,
  "average_profile": 24.02
}, {
  "date": "2016-04-27 15:00",
  "consumption": 19.7,
  "average_profile": 21.65
}, {
  "date": "2016-04-27 16:00",
  "consumption": 19.6,
  "average_profile": 19.95
}, {
  "date": "2016-04-27 17:00",
  "consumption": 26.4,
  "average_profile": 24.86
}, {
  "date": "2016-04-27 18:00",
  "consumption": 22.1,
  "average_profile": 21.52
}, {
  "date": "2016-04-27 19:00",
  "consumption": 20.1,
  "average_profile": 20.24
}, {
  "date": "2016-04-27 20:00",
  "consumption": 14,
  "average_profile": 17.79
}, {
  "date": "2016-04-27 21:00",
  "consumption": 14.5,
  "average_profile": 16.94
}, {
  "date": "2016-04-27 22:00",
  "consumption": 17.3,
  "average_profile": 16.39
}, {
  "date": "2016-04-27 23:00",
  "consumption": 16.7,
  "average_profile": 16.51
}, {
  "date": "2016-04-28 00:00",
  "consumption": 16.3,
  "average_profile": 15.61
}, {
  "date": "2016-04-28 01:00",
  "consumption": 15.8,
  "average_profile": 16.64
}, {
  "date": "2016-04-28 02:00",
  "consumption": 16.2,
  "average_profile": 17.46
}, {
  "date": "2016-04-28 03:00",
  "consumption": 20.3,
  "average_profile": 21.22
}, {
  "date": "2016-04-28 04:00",
  "consumption": 20.5,
  "average_profile": 20.95
}, {
  "date": "2016-04-28 05:00",
  "consumption": 22,
  "average_profile": 24.22
}, {
  "date": "2016-04-28 06:00",
  "consumption": 27,
  "average_profile": 25.12
}, {
  "date": "2016-04-28 07:00",
  "consumption": 21,
  "average_profile": 25.02
}, {
  "date": "2016-04-28 08:00",
  "consumption": 23.8,
  "average_profile": 23.33
}, {
  "date": "2016-04-28 09:00",
  "consumption": 25.4,
  "average_profile": 26.32
}, {
  "date": "2016-04-28 10:00",
  "consumption": 28.1,
  "average_profile": 24.46
}, {
  "date": "2016-04-28 11:00",
  "consumption": 21.8,
  "average_profile": 24.08
}, {
  "date": "2016-04-28 12:00",
  "consumption": 22.3,
  "average_profile": 22.12
}, {
  "date": "2016-04-28 13:00",
  "consumption": 23.4,
  "average_profile": 23.35
}, {
  "date": "2016-04-28 14:00",
  "consumption": 25,
  "average_profile": 24.02
}, {
  "date": "2016-04-28 15:00",
  "consumption": 22.8,
  "average_profile": 21.65
}, {
  "date": "2016-04-28 16:00",
  "consumption": 22.1,
  "average_profile": 19.95
}, {
  "date": "2016-04-28 17:00",
  "consumption": 23.2,
  "average_profile": 24.86
}, {
  "date": "2016-04-28 18:00",
  "consumption": 26.6,
  "average_profile": 21.52
}, {
  "date": "2016-04-28 19:00",
  "consumption": 18.7,
  "average_profile": 20.24
}, {
  "date": "2016-04-28 20:00",
  "consumption": 16.8,
  "average_profile": 17.79
}, {
  "date": "2016-04-28 21:00",
  "consumption": 14.9,
  "average_profile": 16.94
}, {
  "date": "2016-04-28 22:00",
  "consumption": 14.6,
  "average_profile": 16.39
}, {
  "date": "2016-04-28 23:00",
  "consumption": 18,
  "average_profile": 16.51
}, {
  "date": "2016-04-29 00:00",
  "consumption": 16.6,
  "average_profile": 15.61
}, {
  "date": "2016-04-29 01:00",
  "consumption": 17,
  "average_profile": 16.64
}, {
  "date": "2016-04-29 02:00",
  "consumption": 15.4,
  "average_profile": 17.46
}, {
  "date": "2016-04-29 03:00",
  "consumption": 20.8,
  "average_profile": 21.22
}, {
  "date": "2016-04-29 04:00",
  "consumption": 17.6,
  "average_profile": 20.95
}, {
  "date": "2016-04-29 05:00",
  "consumption": 24.8,
  "average_profile": 24.22
}, {
  "date": "2016-04-29 06:00",
  "consumption": 21.9,
  "average_profile": 25.12
}, {
  "date": "2016-04-29 07:00",
  "consumption": 27.1,
  "average_profile": 25.02
}, {
  "date": "2016-04-29 08:00",
  "consumption": 21.3,
  "average_profile": 23.33
}, {
  "date": "2016-04-29 09:00",
  "consumption": 28.1,
  "average_profile": 26.32
}, {
  "date": "2016-04-29 10:00",
  "consumption": 27.3,
  "average_profile": 24.46
}, {
  "date": "2016-04-29 11:00",
  "consumption": 24,
  "average_profile": 24.08
}, {
  "date": "2016-04-29 12:00",
  "consumption": 21.5,
  "average_profile": 22.12
}, {
  "date": "2016-04-29 13:00",
  "consumption": 21.2,
  "average_profile": 23.35
}, {
  "date": "2016-04-29 14:00",
  "consumption": 27.4,
  "average_profile": 24.02
}, {
  "date": "2016-04-29 15:00",
  "consumption": 26.8,
  "average_profile": 21.65
}, {
  "date": "2016-04-29 16:00",
  "consumption": 20.9,
  "average_profile": 19.95
}, {
  "date": "2016-04-29 17:00",
  "consumption": 25.9,
  "average_profile": 24.86
}, {
  "date": "2016-04-29 18:00",
  "consumption": 24.6,
  "average_profile": 21.52
}, {
  "date": "2016-04-29 19:00",
  "consumption": 21.1,
  "average_profile": 20.24
}, {
  "date": "2016-04-29 20:00",
  "consumption": 17.8,
  "average_profile": 17.79
}, {
  "date": "2016-04-29 21:00",
  "consumption": 17.2,
  "average_profile": 16.94
}, {
  "date": "2016-04-29 22:00",
  "consumption": 19.1,
  "average_profile": 16.39
}, {
  "date": "2016-04-29 23:00",
  "consumption": 19.2,
  "average_profile": 16.51
}];
var production_real = [{
  "date": 1440115200,
  "production": 0
}, {
  "date": 1440118800,
  "production": 0
}, {
  "date": 1440122400,
  "production": 0
}, {
  "date": 1440126000,
  "production": 0
}, {
  "date": 1440129600,
  "production": 0
}, {
  "date": 1440133200,
  "production": 10.3
}, {
  "date": 1440136800,
  "production": 66.9
}, {
  "date": 1440140400,
  "production": 162.2
}, {
  "date": 1440144000,
  "production": 205.2
}, {
  "date": 1440147600,
  "production": 251.3
}, {
  "date": 1440151200,
  "production": 339.1
}, {
  "date": 1440154800,
  "production": 376.1
}, {
  "date": 1440158400,
  "production": 327.3
}, {
  "date": 1440162000,
  "production": 370.3
}, {
  "date": 1440165600,
  "production": 276.9
}, {
  "date": 1440169200,
  "production": 211.4
}, {
  "date": 1440172800,
  "production": 138.5
}, {
  "date": 1440176400,
  "production": 59.5
}, {
  "date": 1440180000,
  "production": 3.7
}, {
  "date": 1440183600,
  "production": 0
}, {
  "date": 1440187200,
  "production": 0
}, {
  "date": 1440190800,
  "production": 0
}, {
  "date": 1440194400,
  "production": 0
}, {
  "date": 1440198000,
  "production": 0
}, {
  "date": 1440201600,
  "production": 0
}, {
  "date": 1440205200,
  "production": 0
}, {
  "date": 1440208800,
  "production": 0
}, {
  "date": 1440212400,
  "production": 0
}, {
  "date": 1440216000,
  "production": 0
}, {
  "date": 1440219600,
  "production": 16.6
}, {
  "date": 1440223200,
  "production": 89.1
}, {
  "date": 1440226800,
  "production": 177.3
}, {
  "date": 1440230400,
  "production": 249
}, {
  "date": 1440234000,
  "production": 315.2
}, {
  "date": 1440237600,
  "production": 362.1
}, {
  "date": 1440241200,
  "production": 357.9
}, {
  "date": 1440244800,
  "production": 345.8
}, {
  "date": 1440248400,
  "production": 346.5
}, {
  "date": 1440252000,
  "production": 288.7
}, {
  "date": 1440255600,
  "production": 231.8
}, {
  "date": 1440259200,
  "production": 128.5
}, {
  "date": 1440262800,
  "production": 50.8
}, {
  "date": 1440266400,
  "production": 3.4
}, {
  "date": 1440270000,
  "production": 0
}, {
  "date": 1440273600,
  "production": 0
}, {
  "date": 1440277200,
  "production": 0
}, {
  "date": 1440280800,
  "production": 0
}, {
  "date": 1440284400,
  "production": 0
}, {
  "date": 1440288000,
  "production": 0
}, {
  "date": 1440291600,
  "production": 0
}, {
  "date": 1440295200,
  "production": 0
}, {
  "date": 1440298800,
  "production": 0
}, {
  "date": 1440302400,
  "production": 0.1
}, {
  "date": 1440306000,
  "production": 14.4
}, {
  "date": 1440309600,
  "production": 86.5
}, {
  "date": 1440313200,
  "production": 166.1
}, {
  "date": 1440316800,
  "production": 241.8
}, {
  "date": 1440320400,
  "production": 306.9
}, {
  "date": 1440324000,
  "production": 329.4
}, {
  "date": 1440327600,
  "production": 343
}, {
  "date": 1440331200,
  "production": 371.8
}, {
  "date": 1440334800,
  "production": 320.2
}, {
  "date": 1440338400,
  "production": 271.7
}, {
  "date": 1440342000,
  "production": 208
}, {
  "date": 1440345600,
  "production": 128.6
}, {
  "date": 1440349200,
  "production": 51.1
}, {
  "date": 1440352800,
  "production": 2
}, {
  "date": 1440356400,
  "production": 0
}, {
  "date": 1440360000,
  "production": 0
}, {
  "date": 1440363600,
  "production": 0
}, {
  "date": 1440367200,
  "production": 0
}, {
  "date": 1440370800,
  "production": 0
}, {
  "date": 1440374400,
  "production": 0
}, {
  "date": 1440378000,
  "production": 0
}, {
  "date": 1440381600,
  "production": 0
}, {
  "date": 1440385200,
  "production": 0
}, {
  "date": 1440388800,
  "production": 0
}, {
  "date": 1440392400,
  "production": 16.3
}, {
  "date": 1440396000,
  "production": 79.4
}, {
  "date": 1440399600,
  "production": 157.9
}, {
  "date": 1440403200,
  "production": 230.1
}, {
  "date": 1440406800,
  "production": 311.2
}, {
  "date": 1440410400,
  "production": 317.5
}, {
  "date": 1440414000,
  "production": 339.2
}, {
  "date": 1440417600,
  "production": 331
}, {
  "date": 1440421200,
  "production": 298.8
}, {
  "date": 1440424800,
  "production": 264.3
}, {
  "date": 1440428400,
  "production": 210.1
}, {
  "date": 1440432000,
  "production": 114.3
}, {
  "date": 1440435600,
  "production": 44.1
}, {
  "date": 1440439200,
  "production": 2
}, {
  "date": 1440442800,
  "production": 0
}, {
  "date": 1440446400,
  "production": 0
}, {
  "date": 1440450000,
  "production": 0
}, {
  "date": 1440453600,
  "production": 0
}, {
  "date": 1440457200,
  "production": 0
}, {
  "date": 1440460800,
  "production": 0
}, {
  "date": 1440464400,
  "production": 0
}, {
  "date": 1440468000,
  "production": 0
}, {
  "date": 1440471600,
  "production": 0
}, {
  "date": 1440475200,
  "production": 0
}, {
  "date": 1440478800,
  "production": 11.8
}, {
  "date": 1440482400,
  "production": 66.1
}, {
  "date": 1440486000,
  "production": 153.1
}, {
  "date": 1440489600,
  "production": 226.7
}, {
  "date": 1440493200,
  "production": 303.7
}, {
  "date": 1440496800,
  "production": 310.5
}, {
  "date": 1440500400,
  "production": 331.8
}, {
  "date": 1440504000,
  "production": 326
}, {
  "date": 1440507600,
  "production": 301.6
}, {
  "date": 1440511200,
  "production": 261.4
}, {
  "date": 1440514800,
  "production": 190.9
}, {
  "date": 1440518400,
  "production": 114
}, {
  "date": 1440522000,
  "production": 40.7
}, {
  "date": 1440525600,
  "production": 1.6
}, {
  "date": 1440529200,
  "production": 0
}, {
  "date": 1440532800,
  "production": 0
}, {
  "date": 1440536400,
  "production": 0
}, {
  "date": 1440540000,
  "production": 0
}, {
  "date": 1440543600,
  "production": 0
}, {
  "date": 1440547200,
  "production": 0
}, {
  "date": 1440550800,
  "production": 0
}, {
  "date": 1440554400,
  "production": 0
}, {
  "date": 1440558000,
  "production": 0
}, {
  "date": 1440561600,
  "production": 0
}, {
  "date": 1440565200,
  "production": 10.2
}, {
  "date": 1440568800,
  "production": 63.9
}, {
  "date": 1440572400,
  "production": 166
}, {
  "date": 1440576000,
  "production": 223.3
}, {
  "date": 1440579600,
  "production": 271
}, {
  "date": 1440583200,
  "production": 310
}, {
  "date": 1440586800,
  "production": 343.3
}, {
  "date": 1440590400,
  "production": 311.8
}, {
  "date": 1440594000,
  "production": 296.9
}, {
  "date": 1440597600,
  "production": 233.8
}, {
  "date": 1440601200,
  "production": 208.6
}, {
  "date": 1440604800,
  "production": 109.2
}, {
  "date": 1440608400,
  "production": 36.3
}, {
  "date": 1440612000,
  "production": 1.5
}, {
  "date": 1440615600,
  "production": 0
}, {
  "date": 1440619200,
  "production": 0
}, {
  "date": 1440622800,
  "production": 0
}, {
  "date": 1440626400,
  "production": 0
}, {
  "date": 1440630000,
  "production": 0
}, {
  "date": 1440633600,
  "production": 0
}, {
  "date": 1440637200,
  "production": 0
}, {
  "date": 1440640800,
  "production": 0
}, {
  "date": 1440644400,
  "production": 0
}, {
  "date": 1440648000,
  "production": 0
}, {
  "date": 1440651600,
  "production": 11.3
}, {
  "date": 1440655200,
  "production": 63
}, {
  "date": 1440658800,
  "production": 143.1
}, {
  "date": 1440662400,
  "production": 243.4
}, {
  "date": 1440666000,
  "production": 266.3
}, {
  "date": 1440669600,
  "production": 330.9
}, {
  "date": 1440673200,
  "production": 324.6
}, {
  "date": 1440676800,
  "production": 317.7
}, {
  "date": 1440680400,
  "production": 280.3
}, {
  "date": 1440684000,
  "production": 247.9
}, {
  "date": 1440687600,
  "production": 184.1
}, {
  "date": 1440691200,
  "production": 108.5
}, {
  "date": 1440694800,
  "production": 37
}, {
  "date": 1440698400,
  "production": 3
}, {
  "date": 1440702000,
  "production": 0
}, {
  "date": 1440705600,
  "production": 0
}, {
  "date": 1440709200,
  "production": 0
}, {
  "date": 1440712800,
  "production": 0
}, {
  "date": 1440716400,
  "production": 0
}, {
  "date": 1440720000,
  "production": 0
}, {
  "date": 1440723600,
  "production": 0
}, {
  "date": 1440727200,
  "production": 0
}, {
  "date": 1440730800,
  "production": 0
}, {
  "date": 1440734400,
  "production": 0
}, {
  "date": 1440738000,
  "production": 9.3
}, {
  "date": 1440741600,
  "production": 64.7
}, {
  "date": 1440745200,
  "production": 157.4
}, {
  "date": 1440748800,
  "production": 216.5
}, {
  "date": 1440752400,
  "production": 268.1
}, {
  "date": 1440756000,
  "production": 327.6
}, {
  "date": 1440759600,
  "production": 313.1
}, {
  "date": 1440763200,
  "production": 283.9
}, {
  "date": 1440766800,
  "production": 184
}, {
  "date": 1440770400,
  "production": 137
}, {
  "date": 1440774000,
  "production": 57.4
}, {
  "date": 1440777600,
  "production": 51.3
}, {
  "date": 1440781200,
  "production": 28.7
}, {
  "date": 1440784800,
  "production": 1.2
}, {
  "date": 1440788400,
  "production": 0
}, {
  "date": 1440792000,
  "production": 0
}, {
  "date": 1440795600,
  "production": 0
}, {
  "date": 1440799200,
  "production": 0
}, {
  "date": 1440802800,
  "production": 0
}, {
  "date": 1440806400,
  "production": 0
}, {
  "date": 1440810000,
  "production": 0
}, {
  "date": 1440813600,
  "production": 0
}, {
  "date": 1440817200,
  "production": 0
}, {
  "date": 1440820800,
  "production": 0
}, {
  "date": 1440824400,
  "production": 2.7
}, {
  "date": 1440828000,
  "production": 32.6
}, {
  "date": 1440831600,
  "production": 103.7
}, {
  "date": 1440835200,
  "production": 111.7
}, {
  "date": 1440838800,
  "production": 203.6
}, {
  "date": 1440842400,
  "production": 179.7
}, {
  "date": 1440846000,
  "production": 143.2
}, {
  "date": 1440849600,
  "production": 121.5
}, {
  "date": 1440853200,
  "production": 106.9
}, {
  "date": 1440856800,
  "production": 80.3
}, {
  "date": 1440860400,
  "production": 98.3
}, {
  "date": 1440864000,
  "production": 47.2
}, {
  "date": 1440867600,
  "production": 16
}, {
  "date": 1440871200,
  "production": 0
}, {
  "date": 1440874800,
  "production": 0
}, {
  "date": 1440878400,
  "production": 0
}, {
  "date": 1440882000,
  "production": 0
}, {
  "date": 1440885600,
  "production": 0
}, {
  "date": 1440889200,
  "production": 0
}, {
  "date": 1440892800,
  "production": 0
}, {
  "date": 1440896400,
  "production": 0
}, {
  "date": 1440900000,
  "production": 0
}, {
  "date": 1440903600,
  "production": 0
}, {
  "date": 1440907200,
  "production": 0
}, {
  "date": 1440910800,
  "production": 16
}, {
  "date": 1440914400,
  "production": 83.7
}, {
  "date": 1440918000,
  "production": 162
}, {
  "date": 1440921600,
  "production": 231.6
}, {
  "date": 1440925200,
  "production": 289.5
}, {
  "date": 1440928800,
  "production": 315.5
}, {
  "date": 1440932400,
  "production": 357.4
}, {
  "date": 1440936000,
  "production": 310.6
}, {
  "date": 1440939600,
  "production": 294.8
}, {
  "date": 1440943200,
  "production": 266.8
}, {
  "date": 1440946800,
  "production": 176
}, {
  "date": 1440950400,
  "production": 99.5
}, {
  "date": 1440954000,
  "production": 28.2
}, {
  "date": 1440957600,
  "production": 0.9
}, {
  "date": 1440961200,
  "production": 0
}, {
  "date": 1440964800,
  "production": 0
}, {
  "date": 1440968400,
  "production": 0
}, {
  "date": 1440972000,
  "production": 0
}, {
  "date": 1440975600,
  "production": 0
}];

///generating normal numbers
function generate_normal(mu, sigma) {
  var u = Math.random();
  var v = Math.random();
  var z2 = Math.sqrt(-2 * Math.log(u)) * Math.cos(2 * Math.PI * v);
  var x2 = mu + z2 * sigma;
  return Math.abs(x2);
}
//variable with suffix Consumption regards to TS consumption , without to Production
var dataForPlot = [];
var dataForecastedTillActual = [];
var dataForecastedAfterActual = [];

var dataForPlotConsumption = [];
var dataForecastedTillActualConsumption = [];
var dataForecastedAfterActualConsumption = [];
var dataAverageConsumption = [];

//consumption and production have the same length
for (var i = 0; i < production_real.length; i++) {
  var datetime = new Date(consumption_real[i].date).getTime();
  dataForPlot.push([production_real[i].date * 1000, production_real[i].production]);
  dataForPlotConsumption.push([datetime, consumption_real[i].consumption]);
  dataAverageConsumption.push([datetime, consumption_real[i].average_profile]);
  var hour = moment(production_real[i].date * 1000).get('hour');
  if (hour > 20 || hour < 8) {
    dataForecastedTillActual.push([production_real[i].date * 1000, 0]);
    dataForecastedTillActual.push([production_real[i].date * 1000, production_real[i].production]);
    dataForecastedTillActualConsumption.push([datetime, consumption_real[i].consumption + generate_normal(0, 1)]);
  } else {
    dataForecastedTillActual.push([production_real[i].date * 1000, production_real[i].production + generate_normal(0, 50)]);
    dataForecastedTillActualConsumption.push([datetime, consumption_real[i].consumption + generate_normal(0, 1)]);
    if (i % 6 == 0) {
      dataForecastedTillActual.push([production_real[i].date * 1000, production_real[i].production]);
      dataForecastedTillActualConsumption.push([datetime, consumption_real[i].consumption]);
    }
  }
}

function forecast_data(data, std, isProduction) {
  var forecast = [];
  for (var i = 0; i < data.length; i++) {
    var hour = moment(data[i][0]).get('hour');
    if ((hour > 20 || hour < 8) && isProduction) {
      forecast.push([data[i][0], 0]);
    } else {
      forecast.push([data[i][0], data[i][1] + generate_normal(0, std)]);
    }
  }
  return forecast;
}
//variable with suffix Consumption regards to TS consumption , without to Production
var forecast = forecast_data(dataForPlot, 50, true);
var tab = dataForecastedTillActual.slice(0, 78);
var forecastConsumption = forecast_data(dataForPlotConsumption, 1, false);
var tabConsumption = dataForecastedTillActualConsumption.slice(0, 55);

var simulation4Consumption = Highcharts.chart('container2', {
  colors: ['#7CB5EC', '#434348', '#434348', '#8fdd3c'],
  title: {
    text: 'Tin_Hat_3 Machine Learned Data'
  },
  subtitle: {
    text: 'Consumption'
  },
  chart: {
    animation: {
      duration: 1000
    }
  },
  xAxis: {
    type: 'datetime'
  },
  yAxis: {
    title: {
      text: 'Energy [kWh]'
    }
  },

  plotOptions: {
    line: {
      marker: {
        enabled: false
      }
    }
  },
  series: [{
    name: "Realtime data",
    data: dataForPlotConsumption.slice(0, 2 * 24)
  }, {
    name: "Adapted Forecast",
    data: tabConsumption
  }, {
    name: "Forward Forecast",
    data: forecastConsumption.slice(2 * 24, 3 * 24),
    dashStyle: 'shortdot'
  }, {
    name: "Average Profile",
    data: dataAverageConsumption.slice(0, 2 * 24)
  }]

});

var simulation4 = Highcharts.chart('container', {
  colors: ['#7CB5EC', '#434348', '#434348'],
  title: {
    text: 'Tin_Hat_3 Machine Learned Data'
  },
  subtitle: {
    text: 'Production'
  },
  chart: {
    animation: {
      duration: 1000
    }
  },
  xAxis: {
    type: 'datetime'
  },
  yAxis: {
    title: {
      text: 'Energy [kWh]'
    }
  },

  plotOptions: {
    line: {
      marker: {
        enabled: false
      }
    }
  },
  series: [{
    name: "Realtime data",
    data: dataForPlot.slice(0, 2 * 24)
  }, {
    name: "Adapted Forecast",
    data: tab
  }, {
    name: "Forward Forecast",
    data: forecast.slice(2 * 24, 3 * 24),
    dashStyle: 'shortdot'
  }]

});

var step = 1;
function nextStep() {
  endIdx = 2 * 24 + step;
  if (dataForPlot.length + 1 <= endIdx) return false;
  simulation4.series[0].setData(dataForPlot.slice(0, endIdx + 1), true, true);
  var newForecast = forecast_data(dataForPlot.slice(endIdx, endIdx + 24), 50, true);
  tab.push(newForecast[0]);
  ts_length = simulation4.series[0].data.length;
  if (step % 6 == 0) {
    tab.push([simulation4.series[0].data[ts_length - 1].x, simulation4.series[0].data[ts_length - 1].y]);
  }
  simulation4.series[1].setData(tab, true, true);
  simulation4.series[2].setData(newForecast, true, true);
  step++;
  return true;
}

var refreshIntervalId = 0;

function run() {
  if (dataForPlot.length > simulation4.series[0].data.length) {
    if (refreshIntervalId == 0) refreshIntervalId = setInterval(nextStep, 150);
  }
}

function stop() {
  if (refreshIntervalId != 0) {
    clearInterval(refreshIntervalId);
    refreshIntervalId = 0;
  }
}

function reset() {
  stop();
  step = 0;
  tab = dataForecastedTillActual.slice(0, 78);
  nextStep();
}

$('#stop').click(stop);
$('#reset').click(reset);
$('#update').click(nextStep);
$('#run_1day').click(function () {
  for (var k = 0; k < 24; ++k) if (!nextStep()) break;
});

$('#play').click(run);

var stepConsumption = 1;
function nextStepConsumption() {
  endIdx = 2 * 24 + stepConsumption;
  if (dataForPlotConsumption.length <= endIdx) return false;
  simulation4Consumption.series[0].setData(dataForPlotConsumption.slice(0, endIdx + 1), true, true);
  simulation4Consumption.series[3].setData(dataAverageConsumption.slice(0, endIdx + 1), true, true);
  var newForecast = forecast_data(dataForPlotConsumption.slice(endIdx, endIdx + 24), 1, false);
  tabConsumption.push(newForecast[0]);
  ts_length = simulation4Consumption.series[0].data.length;
  if (stepConsumption % 6 == 0) {
    tabConsumption.push([simulation4Consumption.series[0].data[ts_length - 1].x, simulation4Consumption.series[0].data[ts_length - 1].y]);
  }
  simulation4Consumption.series[1].setData(tabConsumption, true, true);
  simulation4Consumption.series[2].setData(newForecast, true, true);
  stepConsumption++;
  return true;
}

var refreshIntervalIdConsumption = 0;

function runConsumption() {
  if (dataForPlotConsumption.length > simulation4Consumption.series[0].data.length) {
    if (refreshIntervalIdConsumption == 0) refreshIntervalIdConsumption = setInterval(nextStepConsumption, 150);
  }
}

function stopConsumption() {
  if (refreshIntervalIdConsumption != 0) {
    clearInterval(refreshIntervalIdConsumption);
    refreshIntervalIdConsumption = 0;
  }
}

function resetConsumption() {
  stopConsumption();
  stepConsumption = 0;
  tabConsumption = dataForecastedTillActualConsumption.slice(0, 55);
  nextStepConsumption();
}

$('#stop2').click(stopConsumption);
$('#reset2').click(resetConsumption);
$('#update2').click(nextStepConsumption);
$('#run_1day2').click(function () {
  for (var k = 0; k < 24; ++k) if (!nextStepConsumption()) break;
});

$('#play2').click(runConsumption);
// ecar_sim= [
//   [0,30000  ],
//   [50,29760  ],
//   [100,29522  ],
//   [150,29286  ],
//   [200,29051  ],
//   [250,28819  ],
//   [300,28588  ],
//   [350,28360  ],
//   [400,28133  ],
//   [450,27908  ],
//   [500,27685  ],
//   [550,27463  ],
//   [600,27243  ],
//   [650,27025  ],
//   [700,26809  ],
//   [750,26595  ],
//   [800,26382  ],
//   [850,26171  ],
//   [900,25962  ],
//   [950,25754  ],
//   [1000,25548  ],
//   [1050,25343  ],
//   [1100,25141  ],
//   [1150,24940  ],
//   [1200,24740  ],
//   [1250,24542  ],
//   [1300,24346  ],
//   [1350,24151  ],
//   [1400,23958  ],
//   [1450,23766  ],
//   [1500,23576  ],
//   [1550,23387  ],
//   [1600,23200  ],
//   [1650,23015  ],
//   [1700,22831  ],
//   [1750,22648  ],
//   [1800,22535  ],
//   [1850,22422  ],
//   [1900,22310  ],
//   [1950,22198  ],
//   [2000,22087  ],
//   [2050,21977  ],
//   [2100,21867  ],
//   [2150,21758  ],
//   [2200,21649  ],
//   [2250,21541  ],
//   [2300,21433  ],
//   [2350,21326  ],
//   [2400,21219  ],
//   [2450,21113  ],
//   [2500,21008  ],
//   [2550,20903  ],
//   [2600,20798  ],
//   [2650,20694  ],
//   [2700,20591  ],
//   [2750,20488  ],
//   [2800,20385  ],
//   [2850,20283  ],
//   [2900,20182  ],
//   [2950,20081  ],
//   [3000,20001  ],
//   [3050,19921  ],
//   [3100,19841  ],
//   [3150,19762  ],
//   [3200,19683  ],
//   [3250,19604  ],
//   [3300,19525  ],
//   [3350,19447  ],
//   [3400,19369  ],
//   [3450,19292  ],
//   [3500,19215  ],
//   [3550,19138  ],
//   [3600,19061  ],
//   [3650,18985  ],
//   [3700,18909  ],
//   [3750,18834  ],
//   [3800,18758  ],
//   [3850,18683  ],
//   [3900,18608  ],
//   [3950,18534  ],
//   [4000,18460  ],
//   [4050,18386  ],
//   [4100,18313  ],
//   [4150,18239  ],
//   [4200,18130  ],
//   [4250,18021  ],
//   [4300,17913  ],
//   [4350,17805  ],
//   [4400,17699  ],
//   [4450,17592  ],
//   [4500,17487  ],
//   [4550,17382  ],
//   [4600,17278  ],
//   [4650,17174  ],
//   [4700,17071  ],
//   [4750,16969  ],
//   [4800,16867  ],
//   [4850,16766  ],
//   [4900,16665  ],
//   [4950,16565  ],
//   [5000,16466  ],
//   [5050,16367  ],
//   [5100,16269  ],
//   [5150,16090  ],
//   [5200,15913  ],
//   [5250,15738  ],
//   [5300,15564  ],
//   [5350,15393  ],
//   [5400,15224  ],
//   [5450,15056  ],
//   [5500,14891  ],
//   [5550,14727  ],
//   [5600,14565  ],
//   [5650,14405  ],
//   [5700,14246  ],
//   [5750,14090  ],
//   [5800,13935  ],
//   [5850,13781  ],
//   [5900,13630  ],
//   [5950,13480  ],
//   [6000,13332  ]
// ]
// wind_sim=[
//   [0,30000  ],
//   [50,29550  ],
//   [100,29107  ],
//   [150,28670  ],
//   [200,28240  ],
//   [250,27816  ],
//   [300,27399  ],
//   [350,26988  ],
//   [400,26583  ],
//   [450,26185  ],
//   [500,25792  ],
//   [550,25405  ],
//   [600,25024  ],
//   [650,24649  ],
//   [700,24279  ],
//   [750,23915  ],
//   [800,23556  ],
//   [850,23203  ],
//   [900,22855  ],
//   [950,22512  ],
//   [1000,22174  ],
//   [1050,21841  ],
//   [1100,21514  ],
//   [1150,21191  ],
//   [1200,20937  ],
//   [1250,20686  ],
//   [1300,20437  ],
//   [1350,20192  ],
//   [1400,19950  ],
//   [1450,19710  ],
//   [1500,19474  ],
//   [1550,19240  ],
//   [1600,19009  ],
//   [1650,18781  ],
//   [1700,18556  ],
//   [1750,18333  ],
//   [1800,18113  ],
//   [1850,17896  ],
//   [1900,17681  ],
//   [1950,17469  ],
//   [2000,17259  ],
//   [2050,17052  ],
//   [2100,16848  ],
//   [2150,16679  ],
//   [2200,16562  ],
//   [2250,16446  ],
//   [2300,16331  ],
//   [2350,16217  ],
//   [2400,16103  ],
//   [2450,15991  ],
//   [2500,15879  ],
//   [2550,15768  ],
//   [2600,15657  ],
//   [2650,15548  ],
//   [2700,15439  ],
//   [2750,15331  ],
//   [2800,15223  ],
//   [2850,15117  ],
//   [2900,15011  ],
//   [2950,14906  ],
//   [3000,14802  ],
//   [3050,14698  ],
//   [3100,14595  ],
//   [3150,14493  ],
//   [3200,14391  ],
//   [3250,14291  ],
//   [3300,14191  ],
//   [3350,14091  ],
//   [3400,13993  ],
//   [3450,13895  ],
//   [3500,13798  ],
//   [3550,13701  ],
//   [3600,13605  ],
//   [3650,13510  ],
//   [3700,13415  ],
//   [3750,13321  ],
//   [3800,13228  ],
//   [3850,13043  ],
//   [3900,12860  ],
//   [3950,12680  ],
//   [4000,12503  ],
//   [4050,12328  ],
//   [4100,12155  ],
//   [4150,11985  ],
//   [4200,11817  ],
//   [4250,11652  ],
//   [4300,11489  ],
//   [4350,11328  ],
//   [4400,11169  ],
//   [4450,11013  ],
//   [4500,10859  ],
//   [4550,10707  ],
//   [4600,10557  ],
//   [4650,10409  ],
//   [4700,10263  ],
//   [4750,10119  ],
//   [4800,9978  ],
//   [4850,9838  ],
//   [4900,9700  ],
//   [4950,9565  ],
//   [5000,9431  ],
//   [5050,9299  ],
//   [5100,9168  ],
//   [5150,9031  ],
//   [5200,8895  ],
//   [5250,8762  ],
//   [5300,8631  ],
//   [5350,8501  ],
//   [5400,8374  ],
//   [5450,8248  ],
//   [5500,8124  ],
//   [5550,8002  ],
//   [5600,7882  ],
//   [5650,7725  ],
//   [5700,7570  ],
//   [5750,7419  ],
//   [5800,7270  ],
//   [5850,7125  ],
//   [5900,6983  ],
//   [5950,6843  ],
//   [6000,6706  ]
// ]
// ecar_act =[
//   [0,30000  ],
//   [50,30108  ],
//   [100,27771  ],
//   [150,30369  ],
//   [200,28956  ],
//   [250,29153  ],
//   [300,29312  ],
//   [350,26718  ],
//   [400,26611  ],
//   [450,25960  ],
//   [500,26101  ],
//   [550,26571  ],
//   [600,28889  ],
//   [650,28431  ],
//   [700,27957  ],
//   [750,25406  ],
//   [800,27556  ],
//   [850,25755  ],
//   [900,27776  ],
//   [950,27134  ],
//   [1000,23813  ],
//   [1050,24588  ],
//   [1100,23514  ],
//   [1150,24852  ],
//   [1200,23545  ],
//   [1250,23347  ],
//   [1300,23333  ],
//   [1350,23383  ],
//   [1400,22817  ],
//   [1450,24493  ],
//   [1500,22558  ],
//   [1550,23242  ],
//   [1600,22040  ],
//   [1650,23084  ],
//   [1700,23584  ],
//   [1750,23572  ],
//   [1800,21111  ],
//   [1850,21133  ],
//   [1900,23789  ],
//   [1950,23295  ],
//   [2000,22198  ],
//   [2050,21212  ],
//   [2100,23138  ],
//   [2150,21747  ],
//   [2200,21924  ],
//   [2250,21035  ],
//   [2300,20001  ],
//   [2350,19878  ],
//   [2400,21559  ],
//   [2450,21242  ],
//   [2500,20825  ],
//   [2550,19496  ],
//   [2600,20347  ],
//   [2650,20135  ],
//   [2700,19917  ],
//   [2750,21430  ],
//   [2800,19030  ],
//   [2850,19304  ],
//   [2900,18838  ],
//   [2950,19812  ],
//   [3000,19435  ],
//   [3050,20213  ],
//   [3100,20363  ],
//   [3150,18582  ],
//   [3200,20503  ],
//   [3250,19774  ],
//   [3300,19156  ],
//   [3350,19496  ],
//   [3400,19232  ],
//   [3450,20471  ],
//   [3500,18317  ],
//   [3550,19808  ],
//   [3600,18465  ],
//   [3650,17742  ],
//   [3700,19787  ],
//   [3750,19698  ],
//   [3800,20071  ],
//   [3850,17418  ],
//   [3900,19431  ],
//   [3950,19622  ],
//   [4000,17897  ],
//   [4050,18199  ],
//   [4100,17754  ],
//   [4150,18748  ],
//   [4200,17405  ],
//   [4250,18947  ],
//   [4300,19006  ],
//   [4350,18267  ],
//   [4400,17118  ],
//   [4450,18530  ],
//   [4500,16934  ],
//   [4550,17215  ],
//   [4600,18114  ],
//   [4650,17451  ],
//   [4700,16069  ],
//   [4750,17858  ],
//   [4800,17649  ],
//   [4850,17097  ],
//   [4900,17045  ],
//   [4950,17294  ],
//   [5000,16324  ],
//   [5050,15359  ],
//   [5100,16438  ],
//   [5150,16707  ],
//   [5200,15895  ],
//   [5250,14858  ],
//   [5300,16178  ],
//   [5350,15863  ],
//   [5400,16089  ],
//   [5450,15312  ],
//   [5500,14024  ],
//   [5550,13796  ],
//   [5600,15049  ],
//   [5650,14896  ],
//   [5700,14192  ],
//   [5750,13711  ],
//   [5800,14526  ],
//   [5850,13058  ],
//   [5900,12933  ],
//   [5950,13249  ],
//   [6000,13726  ]
// ]
// wind_act=[
//   [0,30000  ],
//   [50,27978  ],
//   [100,27250  ],
//   [150,29341  ],
//   [200,28545  ],
//   [250,28509  ],
//   [300,28720  ],
//   [350,27515  ],
//   [400,25507  ],
//   [450,25572  ],
//   [500,25869  ],
//   [550,26670  ],
//   [600,25927  ],
//   [650,24898  ],
//   [700,24060  ],
//   [750,23972  ],
//   [800,23233  ],
//   [850,21878  ],
//   [900,22503  ],
//   [950,21422  ],
//   [1000,22196  ],
//   [1050,21562  ],
//   [1100,20552  ],
//   [1150,22143  ],
//   [1200,20587  ],
//   [1250,20897  ],
//   [1300,19207  ],
//   [1350,21026  ],
//   [1400,20889  ],
//   [1450,19898  ],
//   [1500,20187  ],
//   [1550,18623  ],
//   [1600,18184  ],
//   [1650,19572  ],
//   [1700,18031  ],
//   [1750,18988  ],
//   [1800,17488  ],
//   [1850,19118  ],
//   [1900,18698  ],
//   [1950,16412  ],
//   [2000,17872  ],
//   [2050,17282  ],
//   [2100,17223  ],
//   [2150,16551  ],
//   [2200,16675  ],
//   [2250,17022  ],
//   [2300,16710  ],
//   [2350,17218  ],
//   [2400,15834  ],
//   [2450,16795  ],
//   [2500,15391  ],
//   [2550,15325  ],
//   [2600,15042  ],
//   [2650,15240  ],
//   [2700,14472  ],
//   [2750,15463  ],
//   [2800,15469  ],
//   [2850,14905  ],
//   [2900,14448  ],
//   [2950,14179  ],
//   [3000,15687  ],
//   [3050,15364  ],
//   [3100,14173  ],
//   [3150,15477  ],
//   [3200,15297  ],
//   [3250,14881  ],
//   [3300,13468  ],
//   [3350,13966  ],
//   [3400,14387  ],
//   [3450,14046  ],
//   [3500,14318  ],
//   [3550,14405  ],
//   [3600,13212  ],
//   [3650,13956  ],
//   [3700,13804  ],
//   [3750,13072  ],
//   [3800,13428  ],
//   [3850,13239  ],
//   [3900,12296  ],
//   [3950,13122  ],
//   [4000,11630  ],
//   [4050,12124  ],
//   [4100,12969  ],
//   [4150,12627  ],
//   [4200,12232  ],
//   [4250,12300  ],
//   [4300,11045  ],
//   [4350,10765  ],
//   [4400,11027  ],
//   [4450,10775  ],
//   [4500,10717  ],
//   [4550,10272  ],
//   [4600,9666  ],
//   [4650,9542  ],
//   [4700,8926  ],
//   [4750,8762  ],
//   [4800,8826  ],
//   [4850,8916  ],
//   [4900,7919  ],
//   [4950,8418  ],
//   [5000,7487  ],
//   [5050,7645  ],
//   [5100,7995  ],
//   [5150,7140  ],
//   [5200,7636  ],
//   [5250,6937  ],
//   [5300,6620  ],
//   [5350,6702  ],
//   [5400,6359  ],
//   [5450,6295  ],
//   [5500,6377  ],
//   [5550,6400  ],
//   [5600,5750  ],
//   [5650,5940  ],
//   [5700,5929  ],
//   [5750,5351  ],
//   [5800,5473  ],
//   [5850,5097  ],
//   [5900,5204  ],
//   [5950,5072  ],
//   [6000,4868  ]
// ]

// var simulation8 =Highcharts.chart('discharge_capacity', {
//  colors: ['#537cba','#3158d2','#cce4ba','#7ab84c'],
//     title: {
//         text: 'Discharge Capacity (mAh) over the number of cycles'
//     },

//     xAxis: {
//         title: {
//             text: 'Cycle Number'
//         }
//     },

//     yAxis: {
//         title: {
//             text: 'Discharge Capacity (mAh)'
//         }
//     },


//     series: [{
//         name: 'E-car charger (simulated)',
//         data: ecar_sim,
//         dashStyle: 'ShortDot'
//     },
//         {
//         name: 'E-car charger (actuals)',
//         data: ecar_act
//     },
//     {
//         name: 'Wind balancing (simulated)',
//         data: wind_sim,
//         dashStyle: 'ShortDot'
//     },

//     {
//         name: 'Wind balancing (actual)',
//         data: wind_act
//     }]

// });
